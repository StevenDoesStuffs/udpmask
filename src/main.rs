use std::error::Error;
use rand_chacha::ChaChaRng;
use rand::prelude::*;
use clap::{Arg, App};
use nfq::*;
use pnet::packet::{
    udp::{
        MutableUdpPacket,
        ipv4_checksum,
        ipv6_checksum
    },
    ipv4::MutableIpv4Packet,
    ipv6::MutableIpv6Packet,
    MutablePacket
};

fn main() -> Result<(), Box<dyn Error>> {

    let args = App::new("udpmask")
        .version("1.0.0")
        .author("Steven Xu <stevendoesstuffs@protonmail.com>")
        .about("In place packet obfuscator")
        .arg(Arg::with_name("key")
            .short("k")
            .long("key")
            .value_name("BASE64")
            .help("Sets the key (base64)")
            .takes_value(true))
        .arg(Arg::with_name("queue-num")
            .short("q")
            .long("queue-num")
            .value_name("NUM")
            .help("Takes the net-filter queue num")
            .takes_value(true)
            .default_value("0"))
        .arg(Arg::with_name("debug")
             .short("d")
             .long("debug")
             .help("Enables debug mode")
             .takes_value(false))
        .get_matches();

    let mut key = [200_u8; 32];
    if let Some(encoded) = args.value_of("key") {
        if base64::decode_config_slice(encoded,
            base64::STANDARD, &mut key)? != 32 {

            panic!("Key must be 256 bits!");
        };
    }

    let (permute, inv) = get_permute(key);
    let queue_num = args.value_of("queue-num").unwrap().parse::<u16>()?;
    let debug = args.is_present("debug");
    macro_rules! dbg {($($tt:tt)*) => {if debug {println!($($tt)*);}}}

    let mut queue = Queue::open()?;
    queue.bind(queue_num)?;
    println!("Started udpmask for queue {}", queue_num);
    loop {
        let mut msg = queue.recv()?;
        let indev = msg.get_indev();
        dbg!("packet! indev - {}; outdev - {}", msg.get_indev(), msg.get_outdev());

        macro_rules! handle {($ver: expr, $packet: ident, $checksum: ident) => {
            if let Some(mut ip) = $packet::new(msg.get_payload_mut()) {
                if ip.get_version() == $ver {

                    let src = ip.get_source();
                    let dest = ip.get_destination();
                    dbg!("ipv{}: src - {}; dest - {}", $ver, src, dest);
                    if let Some(mut udp) = MutableUdpPacket::new(ip.payload_mut()) {

                        dbg!("udp: src - {}; dest - {}, len - {}",
                            udp.get_source(), udp.get_destination(), udp.get_length());
                        if indev == 0 {
                            transform_inv(udp.payload_mut(), &inv);
                        } else {
                            transform(udp.payload_mut(), &permute);
                        }
                        udp.set_checksum($checksum(&udp.to_immutable(), &src, &dest));
                    }
                }
            }
        }}

        handle!(4, MutableIpv4Packet, ipv4_checksum);
        handle!(6, MutableIpv6Packet, ipv6_checksum);

        dbg!();

        msg.set_verdict(Verdict::Accept);
        queue.verdict(msg)?;
    }
}

fn get_permute(key: [u8; 32]) -> ([u8; 256], [u8; 256]) {
    let mut rng = ChaChaRng::from_seed(key);
    let mut permute = [0_u8; 256];
    for i in 0..256 {
        permute[i] = i as u8;
    }
    permute.shuffle(&mut rng);
    let mut inv = [0_u8; 256];
    for i in 0..256 {
        inv[permute[i] as usize] = i as u8;
    }
    (permute, inv)
}

fn transform(data: &mut [u8], permute: &[u8]) {
    // the would be IV, except its too small
    let mut prev = 0;
    for i in 0..data.len() {
        data[i] = permute[(prev ^ data[i]) as usize];
        prev = data[i];
    }
}

fn transform_inv(data: &mut [u8], inv: &[u8]) {
    // the would be IV, except its too small
    let mut prev = 0;
    for i in 0..data.len() {
        let temp = data[i];
        data[i] = inv[data[i] as usize] ^ prev;
        prev = temp;
    }
}
